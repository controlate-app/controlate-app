const React = require('react-native');

const { Dimensions } = React;

const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;

export default {
  container: {
    flex: 1,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginBottom: 20,
  },
  buttons: {
    flex: 1,
    marginBottom: 10,
  },
  button: {
    marginVertical: 10,
  },
  images: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-around',
    width: '100%',
    flexWrap: 'wrap',
  },
  image: {
    width: (deviceWidth - 20) / 3 - 10,
    height: deviceHeight / 8,
    marginTop: 10,
  },
  clearButton: {
    height: 30,
    position: 'absolute',
    top: 0,
    right: 0,
    paddingTop: 3,
    paddingRight: 3,
    margin: 0,
  },
  clearIcon: {
    fontSize: 30,
    lineHeight: 25,
    color: 'red',
    paddingTop: 0,
    marginRight: 0,
  },
};
