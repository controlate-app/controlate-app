import React from "react";
import { View, ImageBackground } from "react-native";
import { Button, Icon, Text } from "native-base";
import * as Permissions from "expo-permissions";
import * as ImagePicker from 'expo-image-picker';
import styles from "./styles";

export default class PhotoPicker extends React.Component {
  constructor(props) {
    super(props);
    const { value, input } = props;
    this.state = { value: value && value.length ? value : input.value };
  }

  pickImage = async () => {
    const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
    if (status === "granted") {
      const selection = await ImagePicker.launchImageLibraryAsync({
        base64: true,
        quality: 0.5
      });

      if (!selection.cancelled) {
        this.photoSelected(selection);
      }
    } else {
      alert("Debes dar permisos para acceder a tus fotos!");
      throw new Error("Debes dar permisos para acceder a tus fotos!");
    }
  };

  takePhoto = async () => {
    const { status } = await Permissions.askAsync(Permissions.CAMERA);
    if (status === "granted") {
      const photo = await ImagePicker.launchCameraAsync({
        base64: true,
        quality: 0.5
      });

      if (!photo.cancelled) {
        this.photoSelected(photo);
      }
    } else {
      alert("Debes dar permisos para acceder a la cámara!");
      throw new Error("Debes dar permisos para acceder a la cámara!");
    }
  };

  clearImage = imageUrl => {
    const { onChange } = this.props;
    if (imageUrl) {
      let { value } = this.state;
      value = value.filter(image => image.uri !== imageUrl);
      this.setState({ value });
      if (value.length > 0) {
        onChange(value);
      } else {
        onChange(null);
      }
    }
  };

  photoSelected = photo => {
    const { onChange } = this.props;
    if (photo && !photo.cancelled) {
      let { value } = this.state;
      value = [...value, photo];
      this.setState({ value });
      onChange(value);
    }
  };

  render() {
    const { input, meta } = this.props;
    const { value } = this.state;
    const hasErrors = meta.invalid && (meta.visited || meta.submitFailed);
    return (
      <View style={styles.container} {...input}>
        <View style={styles.buttons}>
          <Button small iconLeft block onPress={this.takePhoto} style={styles.button}>
            <Icon name="camera"/>
            <Text>Tomar una foto</Text>
          </Button>
          <Button small iconLeft block onPress={this.pickImage}>
            <Icon name="images"/>
            <Text>Seleccionar una foto</Text>
          </Button>
        </View>
        <View style={styles.images}>
          {!value.length ? (
            <Text note>Agrega una imagen para ayudarte a recordar el origen del gasto.</Text>
          ) : (
            value.map(image => (
              <ImageBackground key={image.uri} source={{ uri: image.uri }} style={styles.image}>
                <Button
                  transparent
                  onPress={() => this.clearImage(image.uri)}
                  style={styles.clearButton}
                >
                  <Icon style={styles.clearIcon} name="close"/>
                </Button>
              </ImageBackground>
            ))
          )}
        </View>
        {hasErrors && <Icon name="alert" style={{ color: "red", marginLeft: 3, marginRight: 5 }}/>}
        {hasErrors && <Text style={{ color: "red" }}>{meta.error}</Text>}
      </View>
    );
  }
}
