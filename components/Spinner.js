import React from 'react';
import { Spinner } from 'native-base';

export default () => <Spinner />;
