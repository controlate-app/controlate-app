import React, { Component } from "react";
import Constants from "expo-constants";
import * as Permissions from "expo-permissions";
import * as Location from "expo-location";
import {
  Platform,
  Text,
  StyleSheet,
  Dimensions,
  Animated
} from "react-native";
import MapView, { Marker } from "react-native-maps";
import { Icon, View, Button, Card, CardItem } from "native-base";

import commonColor from "../native-base-theme/variables/commonColor";

const deviceHeight = Dimensions.get("window").height;

const latitudeDelta = 0.005;
const longitudeDelta = 0.005;

const styles = StyleSheet.create({
  resultCard: {
    width: "100%",
    flex: 1,
    justifyContent: "flex-start",
    alignItems: "flex-start",
    flexDirection: "column",
    flexWrap: "wrap",
    zIndex: 999,
    paddingTop: 10,
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.5,
    shadowRadius: 1
  },
  container: {
    width: "100%",
    flex: 1
  },
  card: {
    marginLeft: 0,
    marginRight: 0,
    marginTop: 0,
    marginBottom: 0,
    elevation: 2
  },
  inputGrp: {
    flexDirection: "row",
    borderColor: "transparent"
  },
  map: {
    width: "100%",
    height: "100%",
    flex: 1
  },
  mapContainer: {
    minHeight: deviceHeight / 2,
    width: "100%",
    flex: 1
  },
  markerFixed: {
    position: "absolute",
    left: "50%",
    top: "50%",
    marginTop: -15,
    marginLeft: -7.5
  },
  marker: {
    color: "#F00"
  },
  address_container: {
    marginTop: 10
  },
  address: {
    color: "#333",
    marginTop: 5,
    fontSize: 25
  },
  pinToggler: {
    position: "absolute",
    marginTop: 15,
    marginLeft: 15
  }
});

// const MAP_API_KEY = Constants.platform.android.config.googleMaps.apiKey;
// const MAP_API_KEY = "AIzaSyALBx8s-yanVDiM4UjUrBdO3_DAx6y2xY8";
const MAP_API_KEY = "AIzaSyDQEx-UkJQ7Dif1aiZ3rwQhHveXpX0hszU";

export default class LocationPicker extends Component {
  constructor(props) {
    super(props);

    this.state = {
      startLocation: null,
      error: null,
      location: null,
      address: null,
      searchQuery: null,
      searchResults: [],
      isMoving: false,
      locationType: "ubicacion",
      mapContainerHeight: new Animated.Value(deviceHeight / 3),
      loadingSearchResults: false,
      searchMarker: {
        opacity: 0.0,
        latitude: 0,
        longitude: 0,
        title: "",
        description: ""
      }
    };
  }

  componentDidMount() {
    Location.setApiKey(MAP_API_KEY);
    if (Platform.OS === "android" && !Constants.isDevice) {
      this.setState({
        error: "Oops, this will not work on Sketch in an Android emulator. Try it on your device!"
      });
    } else {
      this.getLocationAsync();
    }
  }

  togglePin = () => {
    if (this.state.locationType === "pin") {
      this.getMyPosAsync();
    } else {
      this.setState({
        locationType: "pin",
        searchMarker: {
          opacity: 0.0,
          latitude: 0,
          longitude: 0,
          title: "",
          description: ""
        }
      });
    }
  };

  getLocationAsync = async () => {
    const { status } = await Permissions.askAsync(Permissions.LOCATION);
    if (status !== "granted") {
      this.setState({
        error: "Debes permitir el acceso a la ubicación."
      });
    }
    const location = await Location.getCurrentPositionAsync();
    this.setState({ startLocation: location });
  };

  onRegionChange = async () => {
    const { locationType, isMoving } = this.state;
    if (locationType === "pin" && isMoving === false) {
      this.setState({
        isMoving: true
      });
    }
  };

  onRegionChangeComplete = region => {
    const { isMoving, locationType } = this.state;
    if (isMoving === true && locationType === "pin") {
      this.setState({ location: region });
      this.fetchLocationData(region.latitude, region.longitude);
    }
  };

  fetchLocationData = async (myLat, myLon) => {
    try {
      const request = {
        headers: {
          "Content-Type": "application/json",
          accept: "application/json"
        }
      };
      let response = await fetch(
        `https://maps.googleapis.com/maps/api/geocode/json?address=${myLat},${myLon}&key=${MAP_API_KEY}`,
        request
      );
      response = await response.json();
      const { results } = response;
      const address = results[0];
      this.locationUpdated(address);
    } catch (error) {
      console.warn(error);
    }
  };

  getMyPosAsync = async () => {
    this.setState({
      locationType: "ubicacion",
      searchMarker: {
        opacity: 0.0,
        latitude: 0,
        longitude: 0,
        title: "",
        description: ""
      }
    });

    this.searchMarker.hideCallout();

    const location = await Location.getCurrentPositionAsync();
    const { coords } = location;
    const newLocation = {
      latitude: coords.latitude,
      longitude: coords.longitude,
      latitudeDelta,
      longitudeDelta
    };

    this.mapView.animateToRegion(newLocation);

    this.setState({
      location: newLocation
    });
    this.fetchLocationData(coords.latitude, coords.longitude);
  };

  locationUpdated = address => {
    this.setState({ address });
    this.props.locationUpdated(address);
    if (this.state.isMoving === true) {
      this.setState({
        isMoving: false
      });
    }
  };

  render() {
    let text = "Actualizando la ubicación ...";
    const { error, startLocation } = this.state;

    if (error) {
      text = error;
    }

    if (startLocation === undefined || !startLocation) {
      return (
        <View>
          <Text>{text}</Text>
        </View>
      );
    }

    const { coords } = startLocation;
    const initialRegion = {
      latitude: coords.latitude,
      longitude: coords.longitude,
      latitudeDelta,
      longitudeDelta
    };

    const {
      address,
      searchMarker,
      locationType,
      isMoving,
      mapContainerHeight
    } = this.state;

    return (
      <View style={styles.container}>
        <View style={styles.container}>
          <Animated.View style={[styles.mapContainer, { minHeight: mapContainerHeight }]}>
            <MapView
              style={[styles.map]}
              ref={ref => this.mapView = ref}
              initialRegion={initialRegion}
              onRegionChange={this.onRegionChange}
              onRegionChangeComplete={this.onRegionChangeComplete}
            >
              <Marker
                ref={ref => this.myMarker = ref}
                coordinate={{ latitude: coords.latitude, longitude: coords.longitude }}
              >
                <View
                  style={{
                    flex: 1,
                    alignItems: "center",
                    justifyContent: "center",
                    width: 35,
                    height: 35,
                    backgroundColor: commonColor.brandPrimary,
                    borderRadius: 17.5
                  }}
                >
                  <Icon name="person" active/>
                </View>
              </Marker>
              <Marker
                ref={ref => this.searchMarker = ref}
                opacity={searchMarker.opacity}
                title={searchMarker.title}
                description={searchMarker.description}
                coordinate={{
                  latitude: searchMarker.latitude,
                  longitude: searchMarker.longitude
                }}
                pinColor="red"
              />
            </MapView>
          </Animated.View>
          <Button onPress={() => this.togglePin()} style={styles.pinToggler}>
            <Icon name={locationType === "pin" ? "expand" : "contract"} active/>
          </Button>
          {locationType === "pin" && (
            <View style={styles.markerFixed}>
              <Icon style={styles.marker} name="pin" active/>
              {!isMoving && address !== null && address !== undefined ? (
                <Card style={{ position: "absolute", width: 200, marginLeft: -93, marginTop: 35 }}>
                  <CardItem>
                    <Text style={{ color: "#000" }}>
                      {address !== null ? address.formatted_address : ""}
                    </Text>
                  </CardItem>
                </Card>
              ) : null}
            </View>
          )}
        </View>
        <Card style={styles.card}>
          <CardItem>
            <Button
              bordered
              full
              iconLeft
              style={{ flex: 1 }}
              onPress={() => this.getMyPosAsync()}
            >
              <Icon type="Ionicons" name="pin"/>
              <Text style={{ color: commonColor.brandPrimary, marginLeft: 5 }}>Usar mi ubicación</Text>
            </Button>
          </CardItem>
        </Card>
      </View>
    );
  }
}
