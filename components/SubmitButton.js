import React from "react";
import { Button, Text } from "native-base";

const styles = {
  buttonStyles: { marginVertical: 30 }
};

export default ({ title, onSubmit, ...rest }) => (
  <Button block primary onPress={onSubmit} style={styles.buttonStyles} {...rest}>
    <Text>{title || "Agregar"}</Text>
  </Button>
);
