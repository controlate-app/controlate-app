import Constants from 'expo-constants';

const ENV = {
  dev: {
    apiUrl: 'https://71f2ff49.ngrok.io/graphql',
    // apiUrl: 'http://controlateapi.app/graphql',
    enableLogging: false,
  },
  staging: {
    apiUrl: 'http://controlateapi.app/graphql',
    enableLogging: false,
  },
  prod: {
    apiUrl: 'http://controlateapi.app/graphql',
    enableLogging: false,
  },
};

function getEnvVars(env = '') {
  if (env.indexOf('staging') !== -1) return ENV.staging;
  if (env.indexOf('prod') !== -1) return ENV.prod;
  return ENV.dev;
}

export default getEnvVars(Constants.manifest.releaseChannel);
