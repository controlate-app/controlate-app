import config from "../config";

const logger = store => next => action => {
  if (config.enableLogging) {
    console.group(action.type);
    console.info("dispatching", action);
    const result = next(action);
    console.log("next state", store.getState());
    console.groupEnd(action.type);
    return result;
  }
  return next(action);
};

export default logger;
