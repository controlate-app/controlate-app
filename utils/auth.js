import { AsyncStorage } from 'react-native';

const AUTH_TOKEN = 'AUTH_TOKEN';
const TOKEN_EXPIRES_IN = 'TOKEN_EXPIRES_IN';

let tokenExpiresIn;
let token;

export const getToken = async () => {
  if (token) {
    return Promise.resolve(token);
  }

  return token = await AsyncStorage.getItem(AUTH_TOKEN);
};

export const signIn = async (newToken, expiresIn) => {
  token = newToken;
  tokenExpiresIn = expiresIn;
  return await AsyncStorage.multiSet([
    [TOKEN_EXPIRES_IN, `expiresIn`],
    [AUTH_TOKEN, newToken]
  ]);
};

export const signOut = async () => {
  token = undefined;
  tokenExpiresIn = undefined;
  return await AsyncStorage.multiRemove([TOKEN_EXPIRES_IN, AUTH_TOKEN]);
};
