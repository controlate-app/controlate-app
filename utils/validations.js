function isNumeric(n) {
  return !isNaN(parseFloat(n)) && isFinite(n);
}

export const required = value => (value ? undefined : 'Requerido');
export const minSize = min => value =>
  value && value.length >= min ? undefined : `Debe tener al menos ${min} dígitos`;
export const maxSize = max => value =>
  !value || value.length <= max ? undefined : `Debe tener como maximo ${max} dígitos`;
export const numeric = value => (isNumeric(value) ? undefined : 'Debe ser un numero');
export const alphaNumeric = value =>
  value && /[^a-zA-Z0-9áéíóúÁÉÍÓU\ ]/i.test(value) ? 'Solo caracteres alfanuméricos' : undefined;
export const email = value =>
  value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)
    ? 'Formato de correo electrónico invalido'
    : undefined;
export const composeValidators = (...validators) => value =>
  validators.reduce((error, validator) => error || validator(value), undefined);

export const mapGQLFormErrors = error => {
  if (Object.keys(error).length && error.graphQLErrors.length > 0) {
    return Object.entries(error.graphQLErrors[0].extensions.validation).reduce(
      (errors, [key, [value]]) => {
        const newKey = key.split('.');
        return { ...errors, [newKey.pop()]: value };
      },
      {},
    );
  }
  return {};
};
