import moment from 'moment';

export const formatDate = date => {
  const newDate = moment(date);
  return newDate.format('dddd D/M/YYYY');
};

export const formatDateTime = date => {
  const newDate = moment(date);
  return newDate.format('dddd D/M/YYYY H:m:s');
};
