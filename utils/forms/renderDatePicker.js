import React from "react";
import { Item, Icon, Label, DatePicker, View, Text } from "native-base";

const styles = {
  helpTextView: { position: "absolute", bottom: -25, left: 0 }
};

function renderErrors(error) {
  return <View style={styles.helpTextView}><Text style={{ color: "red" }}>{error}</Text></View>;
}

export default field => {
  const { input, meta, placeholder, label, icon, withDefaultDate, required = false } = field;
  const hasErrors = meta.invalid && (meta.visited || meta.submitFailed);
  const ph = !withDefaultDate ? (placeholder || new Date().toLocaleDateString("es")) : undefined;
  return (
    <Item stackedLabel error={hasErrors} style={[hasErrors ? { marginBottom: 30 } : {}]}>
      {icon && <Icon active name={icon}/>}
      {label && <Label>{label} {required && <Text style={{ color: 'red' }}>*</Text>}</Label>}
      <DatePicker
        placeHolderText={ph}
        defaultDate={input.value}
        onDateChange={value => input.onChange(value)}
        locale={"es"}
        timeZoneOffsetInMinutes={undefined}
        modalTransparent
        animationType={"slide"}
        androidMode={"default"}
        placeHolderTextStyle={{ color: "grey" }}
        {...input}
      />
      {hasErrors && renderErrors(meta.error)}
    </Item>
  );
};
