import React from 'react';
import PhotoPicker from '../../components/PhotoPicker'

export default ({ input, meta, ...rest }) => (
  <PhotoPicker
    meta={meta}
    input={input}
    onChange={images => input.onChange(images)}
    {...rest}
  />
);
