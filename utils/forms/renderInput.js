import React from "react";
import { Item, Input, Icon, Label, Text } from "native-base";

function renderErrors() {
  return <Icon name="close-circle" ref={ref => this.touchable = ref} />;
}

export default field => {
  const { input, meta, placeholder = null, label, required = false, keyboard = null} = field;
  const hasErrors = meta.invalid && (meta.visited || meta.submitFailed) && !meta.pristine;
  return (
    <Item floatingLabel error={hasErrors}>
      {label && <Label>{label} {required && <Text style={{ color: 'red' }}>*</Text>}</Label>}
      <Input
        keyboardType={keyboard || input.name === 'email' ? "email-address" : "default"}
        secureTextEntry={input.name === 'password'}
        placeholder={meta.active ? placeholder : null}
        autoCapitalize={(input.name === 'email' || input.name === 'password') ? "none" : "sentences"}
        {...input}
      />
      {hasErrors && renderErrors(meta.error)}
    </Item>
  );
};
