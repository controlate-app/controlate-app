import React from 'react';
import LocationPicker from '../../components/LocationPicker'

export default ({ input, meta, ...rest }) => (
  <LocationPicker
    meta={meta}
    input={input}
    locationUpdated={location => input.onChange(location)}
    {...rest}
  />
);
