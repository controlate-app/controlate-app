import React from "react";
import { Item, Icon, Label, Picker, View, Text } from "native-base";

const styles = {
  helpTextView: { position: "absolute", bottom: -25, left: 0 }
};

function renderErrors(error) {
  return <View style={styles.helpTextView}><Text style={{ color: "red" }}>{error}</Text></View>;
}

export default field => {
  const { input, meta, placeholder, label, icon, items, required = false } = field;
  const hasErrors = meta.invalid && (meta.visited || meta.submitFailed);
  return (
    <Item stackedLabel picker error={hasErrors} style={[hasErrors ? { marginBottom: 30 } : {}]}>
      {icon && <Icon active name={icon}/>}
      {label && <Label>{label} {required && <Text style={{ color: 'red' }}>*</Text>}</Label>}
      <Picker
        mode="dropdown"
        iosIcon={<Icon name="arrow-down"/>}
        placeholder={placeholder}
        iosHeader={field.header || "Seleccione"}
        headerBackButtonText="Volver"
        selectedValue={input.value}
        onValueChange={value => input.onChange(value)}
        {...input}
      >
        <Picker.Item key={null} label="Seleccione" value={null}/>
        {items && items.length > 0
          ? items.map(item => <Picker.Item key={item.id} label={item.name} value={item.id}/>)
          : null}
      </Picker>
      {hasErrors && renderErrors(meta.error)}
    </Item>
  );
};
