import { gql } from 'apollo-boost';

export const LOGIN = gql`
  mutation Login($email: Email!, $password: String!) {
    login(email: $email, password: $password) {
      token {
        access_token
        token_type
        expires_in
      }
      user {
        id
        first_name
        last_name
        email
      }
    }
  }
`;

export const LOGOUT = gql`
  mutation Logout($user: String) {
    logout(user: $user) {
      id
      name
    }
  }
`;
