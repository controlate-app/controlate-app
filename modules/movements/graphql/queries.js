import { gql } from 'apollo-boost';

// eslint-disable-next-line import/prefer-default-export
export const MOVEMENTS_BY_MONTH = gql`
  query MovementsByMonth($month: Int!, $year: Int!) {
    movementsByMonth(month: $month, year: $year) {
      id
      user_id
      category_id
      category {
        id
        name
        icon
        type
      }
      title
      payment_method_id
      payment_method {
        id
        name
      }
      date
      time
      amount
      details
      location {
        address
        latitude
        longitude
      }
      created_at
      updated_at
      deleted_at
    }
  }
`;
