import { gql } from 'apollo-boost';

// eslint-disable-next-line import/prefer-default-export
export const CREATE_MOVEMENT = gql`
  mutation CreateMovement(
    $category_id: Int!
    $payment_method_id: Int
    $date: Date
    $time: Time
    $amount: Float!
  ) {
    createMovement(
      movement: {
        category_id: $category_id
        payment_method_id: $payment_method_id
        date: $date
        time: $time
        amount: $amount
      }
    ) {
      id
      user_id
      category_id
      category {
        id
        name
        icon
        type
      }
      title
      payment_method_id
      payment_method {
        id
        name
      }
      date
      time
      amount
      details
      location {
        address
        latitude
        longitude
      }
      created_at
      updated_at
      deleted_at
    }
  }
`;
