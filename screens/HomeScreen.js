import React from 'react';
import MonthMovements from './Movements/MonthMovements';

export default () => <MonthMovements />;
