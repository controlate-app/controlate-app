import React, { Component } from 'react';

import { AppLoading } from 'expo';
import * as Font from 'expo-font';
import { Ionicons } from '@expo/vector-icons';
import { getToken } from '../utils/auth';

const Roboto = require('../node_modules/native-base/Fonts/Roboto.ttf');
const RobotoMedium = require('../node_modules/native-base/Fonts/Roboto_medium.ttf');

export default class AuthLoadingScreen extends Component {
  constructor(props) {
    super(props);

    this.state = {
      userToken: null,
    };
  }

  loadResourcesAsync = async () => {
    let userToken = null;
    await Promise.all([
      Font.loadAsync({
        // This is the font that we are using for our tab bar
        ...Ionicons.font,
        Roboto,
        Roboto_medium: RobotoMedium,
      }),
      (userToken = await getToken()),
    ]);
    this.setState({ userToken });
  };

  handleLoadingError = (error: Error) => {
    // In this case, you might want to report the error to your error reporting
    // service, for example Sentry
    console.warn(error);
  };

  handleFinishLoading = () => {
    const { userToken } = this.state;
    this.props.navigation.navigate(userToken ? 'Main' : 'Auth');
  };

  // Render any loading content that you like here
  render() {
    return (
      <AppLoading
        startAsync={this.loadResourcesAsync}
        onError={this.handleLoadingError}
        onFinish={() => this.handleFinishLoading(this.props.navigation)}
      />
    );
  }
}
