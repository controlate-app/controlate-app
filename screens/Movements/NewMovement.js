import React from 'react';
import PropTypes from 'prop-types';
import { Container, Content } from 'native-base';
import { Alert } from 'react-native';

import { Form } from 'react-final-form';
import { FORM_ERROR } from 'final-form';
import { useMutation } from '@apollo/react-hooks';
import { mapGQLFormErrors } from '../../utils/validations';

import { CREATE_MOVEMENT } from '../../modules/movements/graphql/mutations';
import { MOVEMENTS_BY_MONTH } from '../../modules/movements/graphql/queries';

import Spinner from '../../components/Spinner';

import MovementForm from './newMovement/Form';

const NewMovement = ({ navigation }) => {
  const onSubmit = async (form, createMovement) => {
    const {
      category_id: categoryId,
      payment_method_id: paymentMethodId,
      date,
      amount,
      detail,
      location,
    } = form;
    const data = {
      category_id: categoryId,
      payment_method_id: paymentMethodId || null,
      date: date || null,
      amount,
      detail: detail || null,
      address: location ? location.formatted_address : null,
      latitude: location ? location.geometry.location.lat : null,
      longitude: location ? location.geometry.location.lng : null,
    };
    await createMovement({ variables: data });
  };

  const onError = errors => {
    if (!errors) {
      Alert.alert('Ups, ocurrió un error', 'Intenta nuevamente por favor.');
    }
    const submissionErrors = mapGQLFormErrors(errors);
    return { [FORM_ERROR]: submissionErrors };
  };

  const [createMovement, { loading }] = useMutation(CREATE_MOVEMENT, {
    onCompleted: () => navigation.navigate('Home'),
    onError: error => onError(error),
    update: (cache, { data: { createMovement: movementCreated } }) => {
      const { movementsByMonth } = cache.readQuery({
        query: MOVEMENTS_BY_MONTH,
        variables: { month: 11, year: 2019 },
      });

      cache.writeQuery({
        query: MOVEMENTS_BY_MONTH,
        variables: { month: 11, year: 2019 },
        data: { movementsByMonth: movementsByMonth.concat([movementCreated]) },
      });
    },
  });

  if (loading) {
    return <Spinner />;
  }

  return (
    <Container>
      <Content>
        <Form render={MovementForm} onSubmit={data => onSubmit(data, createMovement)} />
      </Content>
    </Container>
  );
};

NewMovement.propTypes = {
  navigation: PropTypes.shape({
    navigate: PropTypes.func.isRequired,
  }).isRequired,
};

export default NewMovement;
