import React from 'react';
import { SectionList } from 'react-native';
import { Body, Button, Icon, Left, ListItem, Right, Text, H3 } from 'native-base';
import { useQuery } from '@apollo/react-hooks';
import _ from 'lodash';
import { MOVEMENTS_BY_MONTH } from '../../modules/movements/graphql/queries';
import Spinner from '../../components/Spinner';
import { formatDate } from '../../utils/dates';

const renderDivider = date => (
  <ListItem key={date} itemDivider>
    <Text style={{ flex: 1, textAlign: 'center' }}>{formatDate(date)}</Text>
  </ListItem>
);

const renderItem = data => (
  <ListItem icon key={data.id}>
    <Left>
      <Button>
        <Icon active name={data.category_id ? data.category.icon : 'home'} />
      </Button>
    </Left>
    <Body>
      <Text uppercase>{data.category_id ? data.category.name : data.title}</Text>
    </Body>
    <Right>
      <Text style={{ marginRight: 5, color: '#111' }}>${data.amount}</Text>
      <Icon name="arrow-forward" />
    </Right>
  </ListItem>
);

const MonthExpenses = () => {
  const { loading, error, data, refetch } = useQuery(MOVEMENTS_BY_MONTH, {
    variables: { month: 11, year: 2019 },
  });

  if (loading) {
    return <Spinner />;
  }

  if (error) {
    console.log('error', error);
    return <Text>`Error! ${error.message}`</Text>;
  }

  const movementsByDateKey = _.groupBy(data.movementsByMonth, movement => movement.date);

  let movements = [];
  _.forIn(movementsByDateKey, (movementsByDate, date) => {
    movements.push({
      title: date,
      data: movementsByDate,
    });
  });

  movements = _.orderBy(movements, 'title', 'desc');

  return (
    <SectionList
      renderItem={({ item }) => renderItem(item)}
      renderSectionHeader={({ section: { title } }) => renderDivider(title)}
      ListEmptyComponent={<H3 style={{ padding: 20 }}>No registraste movimientos este mes</H3>}
      sections={movements}
      keyExtractor={(item, index) => item + index}
      // initialNumToRender={movements.length}
      progressViewOffset={100}
      onRefresh={() => refetch()}
      refreshing={loading}
    />
  );
};

export default MonthExpenses;
