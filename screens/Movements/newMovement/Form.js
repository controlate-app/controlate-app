import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Field } from 'react-final-form';
import { Form, Button, Item, Label, Segment, Text } from 'native-base';
import SubmitButton from '../../../components/SubmitButton';
import {
  RenderInput,
  RenderPicker,
  RenderLocationPicker,
  RenderDatePicker,
  RenderPhotoPicker,
} from '../../../utils/forms';
import {
  composeValidators,
  required,
  minSize,
  maxSize,
  numeric,
  alphaNumeric,
} from '../../../utils/validations';

const renderLocationComponent = useCurrentLocation => {
  if (useCurrentLocation) {
    return <Field name="location" component={RenderLocationPicker} validate={required} />;
  }
  return <Text>Sin ubicación</Text>;
};

const renderDateTimeSelector = showDateTimeSelector =>
  showDateTimeSelector && (
    <Field
      name="date"
      label="Fecha"
      component={RenderDatePicker}
      validate={composeValidators(required)}
      withDefaultDate
      required
    />
  );

const MovementForm = ({ submitting, pristine, invalid, handleSubmit }) => {
  const [useCurrentLocation, setUseCurrentLocation] = useState(false);
  const [showDateTimeSelector, setShowDateTimeSelector] = useState(false);

  return (
    <Form>
      <Field
        name="category_id"
        label="Categoría"
        placeholder="Seleccione el tipo de gasto"
        items={[
          { id: 1, name: 'Casa' },
          { id: 2, name: 'Auto' },
          { id: 3, name: 'Comida' },
          { id: 4, name: 'Bebida' },
        ]}
        component={RenderPicker}
        validate={composeValidators(required, numeric)}
        required
      />

      <Field
        name="payment_method_id"
        label="Medio de Pago"
        placeholder="Seleccione el medio de pago"
        items={[{ id: 1, name: 'Efectivo' }, { id: 2, name: 'Crédito' }, { id: 3, name: 'Débito' }]}
        component={RenderPicker}
      />

      <Item stackedLabel>
        <Label>Fecha</Label>
        <Segment style={{ backgroundColor: 'transparent' }}>
          <Button
            active={!showDateTimeSelector}
            onPress={() => setShowDateTimeSelector(false)}
            first
          >
            <Text>Fecha Actual</Text>
          </Button>
          <Button active={showDateTimeSelector} onPress={() => setShowDateTimeSelector(true)} last>
            <Text>Seleccionar Fecha</Text>
          </Button>
        </Segment>
        {renderDateTimeSelector(showDateTimeSelector)}
      </Item>

      <Field
        name="amount"
        label="Monto"
        placeholder="$ 14.5"
        component={RenderInput}
        validate={composeValidators(required, numeric, minSize(1), maxSize(8))}
        required
        keyboard="numeric"
      />

      <Field
        name="details"
        label="Detalles"
        placeholder="Asado en lo de tito (opcional)"
        component={RenderInput}
        validate={composeValidators(alphaNumeric, maxSize(250))}
      />

      <Item stackedLabel>
        <Label>Ubicación</Label>
        <Segment style={{ backgroundColor: 'transparent' }}>
          <Button active={useCurrentLocation} onPress={() => setUseCurrentLocation(true)} first>
            <Text>Elegir ubicación</Text>
          </Button>
          <Button active={!useCurrentLocation} onPress={() => setUseCurrentLocation(false)} last>
            <Text>Sin ubicación</Text>
          </Button>
        </Segment>
        {renderLocationComponent(useCurrentLocation)}
      </Item>

      <Item stackedLabel>
        <Label>Imágenes</Label>
        <Field name="images" component={RenderPhotoPicker} />
      </Item>

      <SubmitButton onSubmit={handleSubmit} disabled={submitting || pristine || invalid} />
    </Form>
  );
};

MovementForm.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  submitting: PropTypes.func.isRequired,
  pristine: PropTypes.bool.isRequired,
  invalid: PropTypes.bool.isRequired,
};

export default MovementForm;
