import React from 'react';
import PropTypes from 'prop-types';
import { Container, Content } from 'native-base';
import { Form as FinalForm } from 'react-final-form';
import { useMutation } from '@apollo/react-hooks';
import Spinner from '../../components/Spinner';
import { LOGIN } from '../../modules/auth/graphql/mutations';
import { signIn as setToken } from '../../utils/auth';
import LoginForm from './login/LoginForm';

const signIn = async (navigation, { login: { token, user } }) => {
  const { access_token: accessToken, expires_in: expiresIn } = token;
  await setToken(accessToken, expiresIn);
  navigation.navigate('Main');
};

const Login = ({ navigation }) => {
  const [login, { loading }] = useMutation(LOGIN, {
    onCompleted: async response => signIn(navigation, response),
    onError: error => console.log(error),
  });

  if (loading) {
    return <Spinner />;
  }

  return (
    <Container>
      <Content contentContainerStyle={{ flex: 1, justifyContent: 'center' }}>
        <FinalForm
          onSubmit={formData => login({ variables: formData })}
          render={LoginForm}
          initialValues={{ email: 'nachofassini@gmail.com', password: 'qwerty1234' }}
        />
      </Content>
    </Container>
  );
};

Login.propTypes = {
  navigation: PropTypes.shape({
    navigate: PropTypes.func.isRequired,
  }).isRequired,
};

export default Login;
