import React from 'react';
import PropTypes from 'prop-types';
import { Form } from 'native-base';
import { Field } from 'react-final-form';
import RenderInput from '../../../utils/forms/renderInput';
import { composeValidators, email, maxSize, minSize, required } from '../../../utils/validations';
import SubmitButton from '../../../components/SubmitButton';

const SUBMIT_PRISTINE = true;

const LoginForm = ({ submitting, pristine, invalid, handleSubmit }) => (
  <Form>
    <Field
      name="email"
      label="Email"
      placeholder="Email"
      component={RenderInput}
      validate={composeValidators(required, minSize(6), maxSize(100), email)}
      required
    />
    <Field
      name="password"
      label="Contraseña"
      placeholder="Password"
      component={RenderInput}
      validate={composeValidators(required, minSize(6), maxSize(20))}
      required
    />
    <SubmitButton
      title="Ingresar"
      onSubmit={handleSubmit}
      disabled={submitting || (pristine && !SUBMIT_PRISTINE) || invalid}
    />
  </Form>
);

LoginForm.propTypes = {
  submitting: PropTypes.bool.isRequired,
  pristine: PropTypes.bool.isRequired,
  invalid: PropTypes.bool.isRequired,
  handleSubmit: PropTypes.func.isRequired,
};

export default LoginForm;
