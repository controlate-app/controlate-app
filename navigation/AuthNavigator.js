import { createStackNavigator } from 'react-navigation-stack';

import LoginScreen from '../screens/Auth/Login';

export default createStackNavigator(
  {
    Login: LoginScreen,
  },
  {
    initialRouteName: 'Login',
    headerMode: 'none',
  },
);
