import React from 'react';
import { createStackNavigator } from 'react-navigation-stack';
import { Button, Icon } from 'native-base';
import { Platform } from 'react-native';
import { signOut } from '../utils/auth';
import TabBarIcon from '../components/TabBarIcon';

import HomeScreen from '../screens/HomeScreen';
import NewMovementScreen from '../screens/Movements/NewMovement';

export default createStackNavigator(
  {
    Home: {
      screen: HomeScreen,
      navigationOptions: ({ navigation }) => ({
        headerLeft: (
          <Button
            transparent
            onPress={async () => {
              await signOut();
              // client.resetStore();
              return navigation.navigate('Auth');
            }}
          >
            <Icon name="exit" />
          </Button>
        ),
        title: 'Gastos del Mes',
        headerRight: (
          <Button transparent onPress={() => navigation.navigate('NewMovement')}>
            <Icon name="add" />
          </Button>
        ),
      }),
    },
    NewMovement: {
      screen: NewMovementScreen,
      navigationOptions: () => ({
        title: 'Nuevo Movimiento',
      }),
    },
  },
  {
    navigationOptions: {
      tabBarLabel: 'Home',
      tabBarIcon: ({ focused }) => (
        <TabBarIcon focused={focused} name={Platform.OS === 'ios' ? 'ios-home' : 'md-home'} />
      ),
    },
  },
);
