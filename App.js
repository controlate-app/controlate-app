import React from 'react';

import { Platform, StatusBar } from 'react-native';

import { StyleProvider } from 'native-base';

import { ApolloProvider } from '@apollo/react-hooks';
import { InMemoryCache } from 'apollo-cache-inmemory';
import ApolloClient from 'apollo-boost';
import moment from 'moment';

import getTheme from './native-base-theme/components';
import { getToken, signOut } from './utils/auth';
import config from './config';

import AppNavigator from './navigation/AppNavigator';

const SpanishMoment = require('moment/locale/es');

moment.updateLocale('es', SpanishMoment);

const client = new ApolloClient({
  uri: config.apiUrl,
  cache: new InMemoryCache(),
  request: async operation => {
    const token = await getToken();

    operation.setContext({
      headers: {
        Authorization: token ? `Bearer ${token}` : '',
      },
    });
  },
  onError: error => {
    console.log(error);
    if (
      error &&
      error.graphQLErrors &&
      error.graphQLErrors[0] &&
      error.graphQLErrors[0].debugMessage &&
      error.graphQLErrors[0].debugMessage === 'Unauthenticated.'
    ) {
      console.log('signout');
      signOut();
    }
  },
});

export default () => (
  <StyleProvider style={getTheme()}>
    <ApolloProvider client={client}>
      {Platform.OS === 'ios' && <StatusBar barStyle="default" />}
      <AppNavigator />
    </ApolloProvider>
  </StyleProvider>
);
